import argparse
import logging
import os
from datetime import date, datetime

from dotenv import load_dotenv

logger = logging.getLogger(__name__)


def main(date_from: date):
    logger.info(f"Run on date from '{date_from}'")
    print(f"Run on date from '{date_from}'")


def get_args() -> dict:  # pragma: no cover
    parser = argparse.ArgumentParser()  # NOSONAR - This is user data
    parser.add_argument(
        "--date-from",
        required=False,
        default=os.environ.get("DATE_FROM"),
        help="The date we are running from",
        type=lambda s: datetime.strptime(s, "%Y-%m-%d").date(),
    )
    args_dict = parser.parse_args().__dict__
    return args_dict


if __name__ == "__main__":  # pragma: no cover
    root_dir = os.path.realpath(os.path.dirname(os.path.realpath(__file__)) + "/..")
    load_dotenv(dotenv_path=f"{root_dir}/.env")
    logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO").upper())
    args = get_args()
    main(**args)
